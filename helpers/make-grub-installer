#!/bin/sh
#
#    Copyright (C) 2024  Luis Guzman <ark@switnet.org>
#    Copyright (C) 2022  Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=3
EXTERNAL='deb-src http://ftp.debian.org/debian bookworm main'
REPOKEY=0E98404D386FA1D9
NETINST=true

. ./config

replace grub-efi-amd64-signed grub-efi-amd64

sed 's/shim-signed/shim/' -i grub-installer

cat << EOF | patch --no-backup-if-mismatch -p1
diff -ru a/grub-installer b/grub-installer
--- a/grub-installer	2022-11-25 19:04:53.765346384 -0500
+++ b/grub-installer	2022-11-25 19:16:10.042175329 -0500
@@ -508,8 +508,12 @@
 		grub-efi-ia32-bin grub-efi-ia32
     ;;
     grub-efi*)
+	export DEBIAN_PRIORITY=critical
+	export DEBIAN_FRONTEND=noninteractive
+	unset DEBIAN_HAS_FRONTEND
+	unset DEBCONF_REDIR
 	log-output -t grub-installer \$chroot \$ROOT dpkg -P \\
-		grub grub-legacy grub-pc-bin grub-pc
+		grub grub-legacy grub-pc-bin grub-pc grub-gfxpayload-lists
     ;;
 esac
 
EOF

# Apply newer fixes over stable release (bookworm) using git patches.
for patch in $(ls -v ${DATA}/custom/*.patch)
do
    echo "> Applying $patch"
    patch --no-backup-if-mismatch -Np1 < $patch
done

changelog "Remove grub-efi-amd64-signed dependency. | Non-fatal fail if mkdir efivars fails."

package
