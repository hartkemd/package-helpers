#!/bin/sh
#
#    Copyright (C) 2012-2022  Ruben Rodriguez <ruben@trisquel.info>
#    Copyright (C) 2023 Luis Guzmán <ark@switnet.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=18

. ./config

# Only generate the locale selected at boot
sed 's/keep-existing/keep-existing $LANG/' -i scripts/casper-bottom/14locales

# Make sure we disable ureadahead
sed 's/readahead-list/ureadahead/' -i scripts/casper-bottom/25configure_init

# Enable a11y on mate
sed '/gset org.gnome.desktop.interface toolkit-accessibility true/a \ \ \ \ \ \ \ \ gset org.mate.desktop.interface accessibility true' -i bin/casper-a11y-enable
sed "/gset org.gnome.desktop.interface toolkit-accessibility true/a \ \ \ \ \ \ \ \ gset com.canonical.a11y-profile-manager active-profile \"'blindness\'\"" -i bin/casper-a11y-enable
sed '/gset org.gnome.desktop.interface toolkit-accessibility true/a \ \ \ \ \ \ \ \ gset org.mate.desktop.applications.at screen-reader-enabled true' -i bin/casper-a11y-enable

rm scripts/casper-premount/10driver_updates
rm scripts/casper-bottom/33enable_apport_crashes

# Add localechooser-data dependency
sed 's/Depends:/Depends: localechooser-data,\n/' -i debian/control

# Fix error in lang2locale function
sed "s/cut/cut -d: -f2- | cut/" -i scripts/casper-functions
sed "s/head -n1/sed -n 1p/" -i scripts/casper-functions

# Set trisquel isos as default suggestion.
patch --no-backup-if-mismatch -p1 < $DATA/set_trisquel_iso_suggestion.patch
# FIX T11 only: Set live user uid as 1000, which fails due to
# systemd taking 999 in later versions (LP: #2004092) remove on T12.
patch --no-backup-if-mismatch -p1 < $DATA/set_liveuser_uid_1000.patch

changelog "Compiled for Trisquel"

package
