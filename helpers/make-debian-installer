#!/bin/sh
#
#    Copyright (C) 2009-2020 Ruben Rodriguez <ruben@trisquel.info>
#    Copyright (C) 2023 Luis Guzman <ark@switnet.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=17
EXTERNAL='deb-src http://ftp.debian.org/debian bullseye main'
REPOKEY=0E98404D386FA1D9
NETINST=true
. ./config

# Set kernel from release/main packages.
set_kernel_release() {
KRNL_REL=6.5
LASTKERNEL="$(apt-cache madison -c ${LOCAL_APT}/etc/apt_${CODENAME}.conf linux-hwe-$KRNL_REL-tools-common |\
              grep ${CODENAME}/main |\
              awk '{print$3}'|head -n1 |\
              cut -d. -f1,2,3)"
}

cat << EOF > ${LOCAL_APT}/etc/apt_${CODENAME}.conf
Dir::State "${LOCAL_APT}/var/lib/apt";
Dir::State::status "${LOCAL_APT}/var/lib/dpkg/status";
Dir::Etc::SourceList "${LOCAL_APT}/etc/apt.sources_${CODENAME}.list";
Dir::Etc::SourceParts "";
Dir::Cache "${LOCAL_APT}/var/cache/apt";
pkgCacheGen::Essential "none";
Dir::Etc::Trusted "${LOCAL_APT}/etc/trusted.gpg";
Acquire::ForceIPv4 "true";
EOF

cp $DATA/../keyring.gpg ${LOCAL_APT}/etc/trusted.gpg

cat << EOF > ${LOCAL_APT}/etc/apt.sources_${CODENAME}.list
deb $LOCALMIRROR $CODENAME main
deb $LOCALMIRROR $CODENAME-updates main
deb $LOCALMIRROR $CODENAME-security main
EOF

find build/pkg-lists -type f | xargs -r sed -i /media-retriever/d
apt-get update -c ${LOCAL_APT}/etc/apt_${CODENAME}.conf

# Set kernel release for images
set_kernel_release
sed -i "s|LINUX_KERNEL_ABI ?= .*|LINUX_KERNEL_ABI ?= $LASTKERNEL|" build/config/common
sed -i '/LINUX_KERNEL_ABI/d' build/config/ppc64el.cfg

# Patch MEDIUM_SUPPORTED
patch --no-backup-if-mismatch -p1 < $DATA/medium_supported.patch
for i in amd64 arm64 armhf ppc64el
do
    sed -i "/^KERNELVERSION =/i BASEVERSION = $LASTKERNEL" build/config/$i.cfg
    sed -i 's/^KERNELVERSION = .*/KERNELVERSION = $(BASEVERSION)-generic/g' build/config/$i.cfg
    sed -i '/^KERNELVERSION =/a KERNEL_FLAVOUR = di' build/config/$i.cfg
    sed -i '/^KERNEL_FLAVOUR = di/a KERNELIMAGEVERSION = $(KERNELVERSION)' build/config/$i.cfg
done

##TODO: fix the EFI bootloader image
sed -i '/shim/s|signed|unsigned|g' debian/control
sed -i 's|grub-efi-amd64-signed|grub-efi-amd64-bin|' debian/control
sed -i 's|-signed||' debian/control
sed -i '/win32-loader/d' debian/control

#use unsigned shim
grep -rl "efi\.signed" | xargs -r sed -i 's|efi\.signed|efi|g'
#adjust path and name files from debian to trisquel | $arch-bin efi file (/usr/lib/grub/x86_64-efi/monolithic/grubx64.efi)
sed -i 's|\$platform-signed/grub|\$platform/monolithic/grub|' build/util/efi-image
sed -i 's|\$efi_name-installer.efi|\$efi_name.efi|' build/util/efi-image
##EO-TODO

rm -r build/boot/artwork/11-homeworld
mkdir build/boot/artwork/trisquel/
cp $DATA/splash.svg build/boot/artwork/trisquel/trisquel.svg
sed -i 's|SPLASH_PNG=.*|SPLASH_PNG=boot/artwork/trisquel/trisquel.png|' build/config/x86.cfg
grep -rl 'vshift 8' build/boot | xargs -r sed -i '/vshift/s|8|12|g'
grep -rl 'rows 12' build/boot | xargs -r sed -i '/rows/s|12|10|g'

cat << EOF > build/sources.list.udeb.local
deb $LOCALMIRROR $CODENAME main/debian-installer
deb $LOCALMIRROR $CODENAME-updates main/debian-installer
#deb $LOCALMIRROR $CODENAME-security main/debian-installer
EOF

#firmware-linux-free-udeb is missing
cat << EOF >> build/pkg-lists/netboot/common
#open-ath9k-htc-firmware-udeb
openfwwf-udeb
EOF

rpl "2004-20.." "2004-20$(date +'%y')" . -R
rpl www.ubuntu.com trisquel.info . -R
rpl ubuntu.com trisquel.info build/boot/ -R
rpl www.ubuntu trisquel build/boot/ -R
rpl \"com/ \"info/ build/boot/ -R
rpl Ubuntu Trisquel . -R
rpl UBUNTU TRISQUEL . -R
rpl TRISQUEL-PT UBUNTU-PT . -R
rpl Canonical Trisquel . -R
rpl Ltd\. GNU/Linux . -R
rpl Trisquel-l10n ubuntu-l10n . -R
rpl "Trisquel Ltd, and Rosetta" "Canonical Ltd, and Rosetta" . -R
rpl ubuntu-keyring trisquel-keyring . -R
rpl ubuntu-archive-keyring trisquel-archive-keyring . -R

rpl Debian Trisquel . -R
rpl DEBIAN TRISQUEL . -R
rpl http://ftp.debian.org/debian http://archive.trisquel.org/trisquel . -R
rpl http://www.debian.org/ https://trisquel.info build/boot/ -R

# Replace the debian release for the trisquel one.
rpl bullseye $CODENAME . -R
rpl debian-archive-keyring trisquel-keyring debian/control
rpl debian-ports-archive-keyring trisquel-keyring debian/control
rpl debian-archive-keyring.gpg trisquel-archive-keyring.gpg . -R
rpl debian-ports-archive-keyring.gpg trisquel-archive-keyring.gpg . -R
rpl debian-archive-keyring-udeb trisquel-keyring-udeb . -R
rpl debian-ports-archive-keyring-udeb trisquel-keyring-udeb . -R

# build/config/common
grep -rl TRISQUEL_VERSION |xargs -r sed -i "s|TRISQUEL_VERSION =.*|TRISQUEL_VERSION = $REVISION ($CODENAME)|"

# Disable proposed-updates patch
patch --no-backup-if-mismatch -p1 < $DATA/remove-proposed-updates.patch

echo "# Remove gtk related packages, not looking for graphical installer"
rm $(find build/pkg-lists -type f  -path "*/gtk/*")
echo "Remove unused modules from d-i pkg-list"
grep -rl acpi-modules- build/pkg-lists/ |xargs -r sed -i '/acpi-modules-/d'
grep -rl cdrom-core-modules- build/pkg-lists/ |xargs -r sed -i '/cdrom-core-modules-/d'
grep -rl cdebconf-gtk-entropy build/config/ | xargs -r sed -i 's/cdebconf-gtk-entropy//'

# Ensure that ld-linux is executable
patch --no-backup-if-mismatch -p1 < $DATA/chmod_755_ld-linux2.patch

# HACK: include liblzma.so and liblz4.so from host into iso image, as runtime dependencies of zstd
sed '/not packaged as a udeb/a\\tcp /lib/$(DEB_HOST_MULTIARCH)/liblzma.so.5* $(TREE)/lib/$(DEB_HOST_MULTIARCH)' -i build/Makefile
sed '/not packaged as a udeb/a\\tcp /lib/$(DEB_HOST_MULTIARCH)/liblz4.so* $(TREE)/lib/$(DEB_HOST_MULTIARCH)' -i build/Makefile
sed -i '/Build-Depends:/a\\tliblzma5, liblz4-1,' debian/control

# Fix mini.iso boot on EFI systems
sed 's|/debian|/trisquel|' -i ./build/util/efi-image
sed 's|EFI/debian|EFI/trisquel|' -i ./build/config/arm.cfg ./build/config/x86.cfg

#Fix u-boot naming debian>ubuntu
sed -i 's|.imx|-dtb.imx|g' build/boot/arm/u-boot-image-config

# replace references to the OS as 'Linux' with 'GNU/Linux'
match_rxs=( # ASSERT: aligns with '$replace_rxs'
  'Linux distribution'
  'Linux environment'
  'Unix/Linux system'
  'Linux system'
  'Linux or &debian'
  'DOS, Linux, Macintosh'
)
replace_rxs=( # ASSERT: aligns with '$match_rxs'
  'GNU/Linux distribution'
  'GNU/Linux environment'
  'Unix or GNU/Linux system'
  'GNU/Linux system'
  'GNU/Linux or \&debian'
  'DOS, GNU/Linux, Macintosh'
)
for (( rx_n = 0 ; rx_n < ${#match_rxs[@]} ; ++rx_n ))
do  /bin/sed -i "s|${match_rxs[$rx_n]}|${replace_rxs[$rx_n]}|g ; s|GNU/GNU/|GNU/|g" \
    $(grep -rlI "${match_rxs[$rx_n]}" build/ doc/)
done


# Re apply upstream behavior and configuration features from last release (focal).
# https://git.launchpad.net/ubuntu/+source/debian-installer/log/?h=ubuntu/focal
## - Makebuild set of patches
## https://bazaar.launchpad.net/~xnox/debian-installer/di-focal/changes?filter_path=build/Makefile
for patch in $(ls -v ${DATA}/di_focal/*.diff)
do
    echo "Applying $patch"
    patch --no-backup-if-mismatch -Np0 < $patch
done

changelog "Rebranded and adapted for Trisquel"
package
