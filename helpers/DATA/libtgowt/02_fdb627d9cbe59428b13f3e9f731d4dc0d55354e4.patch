From fdb627d9cbe59428b13f3e9f731d4dc0d55354e4 Mon Sep 17 00:00:00 2001
From: Nicholas Guriev <nicholas@guriev.su>
Date: Thu, 4 May 2023 16:21:09 +0300
Subject: [PATCH] New Unbundle-libSRTP.patch

Link against the updated library from the libsrtp2-dev package which already
has a fix of OpenSSL 3.0.0 incompatibility.

Gbp-Dch: full
---
 debian/control                        |   1 +
 debian/patches/Unbundle-libSRTP.patch | 124 ++++++++++++++++++++++++++
 debian/patches/series                 |   1 +
 3 files changed, 126 insertions(+)
 create mode 100644 debian/patches/Unbundle-libSRTP.patch

diff --git a/debian/control b/debian/control
index c9aa2912..516aa47e 100644
--- a/debian/control
+++ b/debian/control
@@ -20,6 +20,7 @@ Build-Depends-Arch:
  libpipewire-0.3-dev,
  libprotobuf-dev,
  libpulse-dev,
+ libsrtp2-dev,
  libssl-dev,
  libswresample-dev,
  libswscale-dev,
diff --git a/debian/patches/Unbundle-libSRTP.patch b/debian/patches/Unbundle-libSRTP.patch
new file mode 100644
index 00000000..466ad0ff
--- /dev/null
+++ b/debian/patches/Unbundle-libSRTP.patch
@@ -0,0 +1,124 @@
+Description: Avoid private symbols and link against system-wide libSRTP
+ The package no longer uses outdated bundled copy of the library. The change
+ fixes incompatibility with OpenSSL 3.0.0 or later.
+ .
+ The excluded code in SrtpSession looks unreachable from the call integration
+ in Telegram Desktop. Though, I can't 100% confirm this.
+Author: Nicholas Guriev <guriev-ns@ya.ru>
+Forwarded: https://github.com/desktop-app/tg_owt/pull/123
+Last-Update: Thu, 04 May 2023 16:21:09 +0300
+
+--- a/CMakeLists.txt
++++ b/CMakeLists.txt
+@@ -2658,6 +2658,9 @@ if (TG_OWT_USE_PROTOBUF)
+     list(APPEND export_targets proto)
+ endif()
+ 
++if (LIBSRTP_FOUND)
++    target_compile_definitions(tg_owt PRIVATE HAVE_LIBSRTP)
++endif()
+ if (NOT absl_FOUND)
+     include(cmake/libabsl.cmake)
+     list(APPEND export_targets libabsl)
+--- a/cmake/libsrtp.cmake
++++ b/cmake/libsrtp.cmake
+@@ -1,3 +1,16 @@
++find_package(PkgConfig REQUIRED)
++pkg_check_modules(LIBSRTP libsrtp2)
++
++if (LIBSRTP_FOUND)
++    add_library(libsrtp INTERFACE EXCLUDE_FROM_ALL)
++    add_library(tg_owt::libsrtp ALIAS libsrtp)
++
++    target_include_directories(libsrtp INTERFACE ${LIBSRTP_INCLUDE_DIRS} ${LIBSRTP_CFLAGS_OTHER})
++    target_link_libraries(libsrtp INTERFACE ${LIBSRTP_LINK_LIBRARIES} ${LIBSRTP_LDFLAGS_OTHER})
++
++    return()
++endif()
++
+ add_library(libsrtp OBJECT EXCLUDE_FROM_ALL)
+ init_target(libsrtp)
+ add_library(tg_owt::libsrtp ALIAS libsrtp)
+--- a/src/pc/external_hmac.cc
++++ b/src/pc/external_hmac.cc
+@@ -15,7 +15,6 @@
+ 
+ #include "rtc_base/logging.h"
+ #include "rtc_base/zero_memory.h"
+-#include "third_party/libsrtp/include/srtp.h"
+ 
+ // Begin test case 0 */
+ static const uint8_t kExternalHmacTestCase0Key[20] = {
+--- a/src/pc/external_hmac.h
++++ b/src/pc/external_hmac.h
+@@ -30,9 +30,12 @@
+ 
+ #include <stdint.h>
+ 
+-#include "third_party/libsrtp/crypto/include/crypto_types.h"
+-#include "third_party/libsrtp/include/srtp.h"
+-#include "third_party/libsrtp/include/srtp_priv.h"
++#ifdef HAVE_LIBSRTP
++# include <srtp2/auth.h>
++# include <srtp2/srtp.h>
++#else
++# include "srtp_priv.h"
++#endif
+ 
+ #define EXTERNAL_HMAC_SHA1 SRTP_HMAC_SHA1 + 1
+ #define HMAC_KEY_LENGTH 20
+--- a/src/pc/srtp_session.cc
++++ b/src/pc/srtp_session.cc
+@@ -30,8 +30,12 @@
+ #include "rtc_base/thread_annotations.h"
+ #include "rtc_base/time_utils.h"
+ #include "system_wrappers/include/metrics.h"
+-#include "third_party/libsrtp/include/srtp.h"
+-#include "third_party/libsrtp/include/srtp_priv.h"
++
++#ifdef HAVE_LIBSRTP
++# include <srtp2/srtp.h>
++#else
++# include "srtp_priv.h"
++#endif
+ 
+ namespace cricket {
+ 
+@@ -290,6 +294,7 @@ bool SrtpSession::UnprotectRtcp(void* p,
+ bool SrtpSession::GetRtpAuthParams(uint8_t** key, int* key_len, int* tag_len) {
+   RTC_DCHECK(thread_checker_.IsCurrent());
+   RTC_DCHECK(IsExternalAuthActive());
++#ifndef HAVE_LIBSRTP
+   if (!IsExternalAuthActive()) {
+     return false;
+   }
+@@ -313,6 +318,10 @@ bool SrtpSession::GetRtpAuthParams(uint8
+   *key_len = external_hmac->key_length;
+   *tag_len = rtp_auth_tag_len_;
+   return true;
++#else
++  RTC_LOG_F(LS_WARNING) << "unavailable";
++  return false;
++#endif
+ }
+ 
+ int SrtpSession::GetSrtpOverhead() const {
+@@ -336,6 +345,7 @@ bool SrtpSession::GetSendStreamPacketInd
+                                            int in_len,
+                                            int64_t* index) {
+   RTC_DCHECK(thread_checker_.IsCurrent());
++#ifndef HAVE_LIBSRTP
+   srtp_hdr_t* hdr = reinterpret_cast<srtp_hdr_t*>(p);
+   srtp_stream_ctx_t* stream = srtp_get_stream(session_, hdr->ssrc);
+   if (!stream) {
+@@ -346,6 +356,10 @@ bool SrtpSession::GetSendStreamPacketInd
+   *index = static_cast<int64_t>(rtc::NetworkToHost64(
+       srtp_rdbx_get_packet_index(&stream->rtp_rdbx) << 16));
+   return true;
++#else
++  RTC_LOG_F(LS_WARNING) << "unavailable";
++  return false;
++#endif
+ }
+ 
+ bool SrtpSession::DoSetKey(int type,
#diff --git a/debian/patches/series b/debian/patches/series
#index 7dc5c619..0dd4e655 100644
#--- a/debian/patches/series
#+++ b/debian/patches/series
#@@ -3,4 +3,5 @@ Convert-endianness.patch
# Fix-libabsl-include.patch
# Packaged-PipeWire.patch
# Skip-RNNoise.patch
#+Unbundle-libSRTP.patch
# Backport-to-stable-libvpx.patch
diff --git a/debian/patches/series b/debian/patches/series
index fefeb174..bc834658 100644
--- a/debian/patches/series
+++ b/debian/patches/series
@@ -4,5 +4,6 @@ Fix-libabsl-include.patch
 Packaged-PipeWire.patch
 Prefer-std-bit-over-absl-bits.patch
 Skip-RNNoise.patch
+Unbundle-libSRTP.patch
 Backport-to-stable-Abseil.patch
 Backport-to-stable-libvpx.patch
-- 
GitLab

